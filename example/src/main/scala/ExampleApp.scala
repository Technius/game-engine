package example

import java.util.UUID
import co.technius.gameengine._

import example.{ Component => C }

object ExampleApp {
  def main(args: Array[String]): Unit = {
    println("hello world!")
    val startState: Map[UUID, Seq[Component]] = Map(
      UUID.randomUUID() -> Seq(
        C.Position(0, 0),
        C.Velocity(100, 100)
      )
    )
    val env = game(startState)
    println(env.snapshot)
    env.run()
    println(env.snapshot)
  }

  def game(state: Map[UUID, Seq[Component]]) =
    Environment[Algebra](Seq(Systems.movement), initialState = state) { (entities, events) =>
      val entityEventHandler = onEntityEvent(entities)
      events foreach {
        case EntityTarget(target, event) => entityEventHandler(target, event)
        case _ => // TODO: Entity events?
      }
    }

  def onEntityEvent(entities: MEntityMap[Algebra]) = (target: UUID, event: EntityEvent) => {
    import EntityEvent._
    event match {
      case MoveTowards(x, y) =>
        for {
          posMap <- entities.allWith[C.Position]
          pos    <- posMap.get(target)
        } {
          posMap(target) = pos.copy(x = x, y = y)
        }
    }
  }
}
