package example

import java.util.UUID

import co.technius.gameengine.SimAlgebra

sealed trait Component
object Component {
  case class Position(x: Int, y: Int) extends Component
  case class Velocity(x: Int, y: Int) extends Component
}

sealed trait Event
case class EntityTarget(target: UUID, event: EntityEvent) extends Event

sealed trait EntityEvent
object EntityEvent {
  case class MoveTowards(x: Int, y: Int) extends EntityEvent
}

trait Algebra extends SimAlgebra {
  type Component = example.Component
  type Event = example.Event
}
object Algebra extends Algebra
