package example

import co.technius.gameengine._
import example.{ Component => C }

object Systems {
  val movement = System[Algebra] { snapshot =>
    snapshot.select2[C.Position, C.Velocity] { (id, p, v) =>
      EntityTarget(id, EntityEvent.MoveTowards(p.x + v.x, p.y + v.y))
    }
  }
}
