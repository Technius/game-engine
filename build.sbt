val projectName = "game-engine"

/*
 * Common settings
 */

val libGdxVersion = "1.9.2"

scalaVersion in ThisBuild := "2.12.1"
scalacOptions in ThisBuild ++= Seq(
  "-deprecation",
  "-unchecked",
  "-feature",
  "-Xfatal-warnings",
  "-Yno-adapted-args",
  "-Xlint",
  "-Xfuture"
)

lazy val sharedSettings = Seq(
  version := "0.0.1-SNAPSHOT",
  organization := "co.technius"
)

/* 
 * Project definitions
 */

lazy val root =
  (project in file("."))
    .aggregate(core, graphics, desktop, example)

lazy val core =
  (project in file("core"))
    .settings(sharedSettings: _*)
    .settings(
      name := projectName + "-core"
    )

lazy val graphics =
  (project in file("graphics"))
    .settings(sharedSettings: _*)
    .settings(
      name := projectName + "-graphics"
    )
    .dependsOn(core)

lazy val desktop =
  (project in file("desktop"))
    .settings(sharedSettings: _*)
    .settings(
      name := projectName + "-desktop",
      fork in run := true
    )
    .dependsOn(core, graphics)

lazy val example =
  (project in file("example"))
    .settings(sharedSettings: _*)
    .settings(
      name := projectName + "-example",
      run in Compile := {
        (run in Compile).evaluated
      }
    )
    .dependsOn(desktop)
