Maybe not `pipeline`, but `environment`? On each tick:
* a snapshot of the environment is taken
* possible actions to perform (events) are generated based on the environment
* events are then evaluated by the environment
