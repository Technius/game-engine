package co.technius.gameengine

import java.util.UUID
import scala.collection.mutable.HashMap

/**
 * Describes how the engine should run the simulation on each frame.
 */
trait Environment[A <: SimAlgebra] {

  def systems: Seq[System[A]]

  def snapshot: Snapshot[A]

  def processEvents: (MEntityMap[A], Seq[A#Event]) => Unit

  def run(): Unit
}

object Environment {
  def apply[A <: SimAlgebra](
      syss: Seq[System[A]],
      initialState: Map[UUID, Seq[A#Component]] = Map.empty
    )(handler: (MEntityMap[A], Seq[A#Event]) => Unit): Environment[A] =
    new Environment[A] {
      val entities: MEntityMap[A] = HashMap.empty
      initialState foreach { case (id, components) =>
        components foreach { c =>
          val clz = c.getClass.asInstanceOf[Class[A#Component]]
          entities.getOrElseUpdate(clz, HashMap()).update(id, c)
        }
      }

      def processEvents = handler

      def systems = syss

      def snapshot = Snapshot[A](entities.mapValues(_.toMap).toMap)

      def run() = {
        val ss = snapshot
        val events = systems.foldRight(Seq.empty[A#Event])((sys, acc) => sys.run(ss) ++ acc)
        processEvents(entities, events)
      }
    }
}
