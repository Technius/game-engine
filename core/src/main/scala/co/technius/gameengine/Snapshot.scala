package co.technius.gameengine

import java.util.UUID
import scala.collection.immutable.Seq
import scala.reflect.{ classTag, ClassTag }


/**
 * A representation of all entities in a simulation at a single moment.
 */
case class Snapshot[Alg <: SimAlgebra](components: EntityMap[Alg]) {
  private[gameengine] type Component = Alg#Component
  private[gameengine] type Event = Alg#Event

  def entitiesWith[C <: Component: ClassTag]: Map[UUID, C] = {
    val cls = classTag[C].runtimeClass
    val opt: Option[Map[UUID, C]] = components.collectFirst {
      case (c, comps) if cls == c => comps.asInstanceOf[Map[UUID, C]]
    }
    opt.getOrElse(Map())
  }

  def select[C <: Component: ClassTag](f: (UUID, C) => Event): Seq[Event] =
    entitiesWith[C]
      .map { case (id, component) => f(id, component) }
      .to[Seq]

  def select2[C <: Component: ClassTag, D <: Component: ClassTag](f: (UUID, C, D) => Event): Seq[Event] = {
    val withC = entitiesWith[C]
    val withD = entitiesWith[D]
    val events = for {
      (id, c) <- withC
      d       <- withD.get(id)
    } yield {
      f(id, c, d)
    }

    events.to[Seq]
  }

  def findEntity(id: UUID): Seq[Component] =
    components
      .flatMap { case (_, comps) => comps.get(id) }
      .to[Seq]
}
