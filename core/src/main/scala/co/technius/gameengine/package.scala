package co.technius

import scala.collection.Map
import scala.collection.immutable.{ Map => IMap }
import scala.collection.mutable.HashMap
import scala.language.{ higherKinds, implicitConversions }
import scala.reflect.{ classTag, ClassTag }
import java.util.UUID

package object gameengine extends Implicits {
  type BaseEntityMap[A <: SimAlgebra, M[K, V] <: Map[K, V]] =
    M[Class[_ <: A#Component], M[UUID, A#Component]]

  type EntityMap[A <: SimAlgebra] = BaseEntityMap[A, IMap]
  type MEntityMap[A <: SimAlgebra] = BaseEntityMap[A, HashMap]

  val EntityMap = Map
  val MEntityMap = HashMap

  implicit class EntityMapOps[A <: SimAlgebra, M[K,V] <: Map[K,V], B <: BaseEntityMap[A, M]](val e: B) extends AnyVal{

    def getC[C <: A#Component: ClassTag](id: UUID): Option[C] = {
      for {
        comps <- allWith[C]
        comp  <- comps.get(id)
      } yield {
        comp.asInstanceOf[C]
      }
    }

    def allWith[C <: A#Component: ClassTag]: Option[M[UUID, C]] = {
      val clz = classTag[C].runtimeClass.asInstanceOf[Class[C]]
      e.get(clz).map(_.asInstanceOf[M[UUID, C]])
    }
  }
}
