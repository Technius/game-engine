package co.technius.gameengine

/**
 * Determines how to generate a sequence of events based on a given snapshot
 */
case class System[A <: SimAlgebra](run: Snapshot[A] => Seq[A#Event]) extends AnyVal {

  /**
   * Creates a new system that returns the combined events of this system
   * and the other system.
   */
  def mergeResult(other: System[A]): System[A] =
    System[A](snapshot => run(snapshot) ++ other.run(snapshot))

  /**
   * Filters the resulting events based on a condition
   */
  def filter(cond: A#Event => Boolean): System[A] =
    System[A](snapshot => run(snapshot).filter(cond))
}

object System {

  def a[A<: SimAlgebra] = System[A] { snapshot =>
    Seq()
  }
}
